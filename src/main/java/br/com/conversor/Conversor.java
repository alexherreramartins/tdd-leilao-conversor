package br.com.conversor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Conversor {

    public static String conversorDeNumero(Integer numeroNatural) {

        Algarismo[] algarismos = Algarismo.values();
        List<Algarismo> resultados = new ArrayList<>();


        for (Algarismo algarismo : algarismos) {

            if (numeroNatural == algarismo.getValor()){
                resultados.add(algarismo);
                break;
            }else{
                if (numeroNatural >= algarismo.getValor()) {
                    int d = numeroNatural % algarismo.getValor();
                    if (d == 0){
                        for (int i=0;i<numeroNatural;i++){
                            resultados.add(algarismo);
                        }
                    }
                    else{
                        resultados.add(algarismo);
                    }
                    numeroNatural = d;
                }
            }

        }


        return resultadoInterpretado(resultados);

    }

    private static String resultadoInterpretado(List<Algarismo> resultados) {
        String resposta = "";

        Map<Object,List<Algarismo>> s =  resultados.stream().collect(Collectors.groupingBy(w -> w.name()));

        for (Algarismo algarismo : Algarismo.values() ){
            List<Algarismo> saida = s.get(algarismo.name());
            if (saida != null){
                if (algarismo.name() == "I" && saida.size() == 4){
                    resposta += "IV";
                    break;
                }
                else {
                    for (Algarismo alg : saida) {
                        resposta += alg.name();
                    }
                }
            }
        }

        return resposta;
    }

}


