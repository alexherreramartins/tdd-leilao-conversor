package br.com.leilao;

public class Lance {
    private Usuario usuario;
    private Double valorDoLance;

    public Lance(Usuario usuario, double valorDoLance) {
        this.usuario = usuario;
        this.valorDoLance = valorDoLance;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Double getValorDoLance() {
        return valorDoLance;
    }

    public void setValorDoLance(Double valorDoLance) {
        this.valorDoLance = valorDoLance;
    }
}
