package br.com.leilao;

import java.util.ArrayList;
import java.util.List;

public class Leilao {
    private List<Lance> lances;

    public Leilao() {
        this.lances = new ArrayList<Lance>();
    }

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    public void adicionarLance(Lance lance) {
        int tamanho = this.lances.size();

        if ( tamanho == 0 || this.lances.get(tamanho - 1).getValorDoLance() < lance.getValorDoLance()) {
            this.lances.add(lance);
        } else {
            throw new RuntimeException("Valor do lance nao permitido");
        }
    }
}

