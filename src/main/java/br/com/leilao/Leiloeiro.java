package br.com.leilao;

public class Leiloeiro {
    private String nome;
    private Leilao leilao;

    public Leiloeiro(Leilao leilao) {
        this.leilao = leilao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public double getMaiorLance() {

        return this.leilao.getLances().get(this.leilao.getLances().size() - 1).getValorDoLance();
    }

    public Usuario getQuemMaiorLance() {

        return this.leilao.getLances().get(this.leilao.getLances().size() - 1).getUsuario();
    }


}
