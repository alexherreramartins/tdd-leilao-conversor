package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LeiloeiroTeste {

    @Test
    public void TestarRetornaMaiorLance(){

        Leilao leilao =  new Leilao();
        Usuario usuario = new Usuario(1,"Teste");

        Lance lance = new Lance(usuario, 100.00);
        leilao.adicionarLance(lance);

        Lance lance2 = new Lance(usuario, 130.00);
        leilao.adicionarLance(lance2);

        Lance lance3 = new Lance(usuario, 1000.00);
        leilao.adicionarLance(lance3);


        Leiloeiro leiloeiro = new Leiloeiro(leilao);

        Assertions.assertEquals(1000.00, leiloeiro.getMaiorLance());

    }

    @Test
    public void TestarRetornaQuemMaiorLance(){

        Leilao leilao =  new Leilao();
        Usuario usuario = new Usuario(1,"Teste");

        Lance lance = new Lance(usuario, 100.00);
        leilao.adicionarLance(lance);

        Lance lance2 = new Lance(usuario, 130.00);
        leilao.adicionarLance(lance2);

        Lance lance3 = new Lance(usuario, 1000.00);
        leilao.adicionarLance(lance3);


        Leiloeiro leiloeiro = new Leiloeiro(leilao);

        Assertions.assertEquals(usuario, leiloeiro.getQuemMaiorLance());

    }
}
