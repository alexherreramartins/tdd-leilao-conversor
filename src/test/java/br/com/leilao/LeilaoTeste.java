package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LeilaoTeste {

    @Test
    public void TestarAdicionarLance(){

        Leilao leilao =  new Leilao();
        Usuario usuario = new Usuario(1,"Teste");

        Lance lance = new Lance(usuario, 100.00);

        leilao.adicionarLance(lance);

        Assertions.assertEquals(1,leilao.getLances().size());
    }


    @Test
    public void TestarAdicionarLanceMenor(){

        Leilao leilao =  new Leilao();
        Usuario usuario = new Usuario(1,"Teste");

        Lance lance = new Lance(usuario, 100.00);
        leilao.adicionarLance(lance);

        Lance lance2 = new Lance(usuario, 50.00);

        Assertions.assertThrows(RuntimeException.class, () -> {leilao.adicionarLance(lance2);},"Valor do lance nao permitido");

    }
}
