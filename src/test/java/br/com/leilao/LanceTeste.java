package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LanceTeste {

    @Test
    public void TestarCriarLance(){
        Usuario usuario = new Usuario(1,"Teste");
        Lance lance =  new Lance(usuario, 1000.00);

        Assertions.assertNotEquals(null, lance);
    }

    @Test
    public void TestarGetLance(){
        Usuario usuario = new Usuario(1,"Teste");
        Lance lance =  new Lance(usuario, 1000.00);

        Assertions.assertEquals(1000.00, lance.getValorDoLance());
    }

    @Test
    public void TestarGetUsuarioLance(){
        Usuario usuario = new Usuario(1,"Teste");
        Lance lance =  new Lance(usuario, 1000.00);

        Assertions.assertEquals(usuario, lance.getUsuario());
    }
}
