package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UsuarioTeste {

    @Test
    public void TestarGetNome(){
        Usuario usuario = new Usuario(1,"Teste");

        Assertions.assertEquals("Teste", usuario.getName());
    }

    @Test
    public void TestarGetId(){
        Usuario usuario = new Usuario(1,"Teste");

        Assertions.assertEquals(1, usuario.getId());
    }


}
