package br.com.conversor;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ConversorTeste {

    @Test
    public void testarConversorDeNumeroCom4(){

        Assertions.assertEquals("XVIV", Conversor.conversorDeNumero(19));
    }

    @Test
    public void testarConversorDeNumeroCheio(){

        Assertions.assertEquals("L", Conversor.conversorDeNumero(50));
    }

    @Test
    public void testarConversorDeNumeroComDoisAlgoritmos(){

        Assertions.assertEquals("XV", Conversor.conversorDeNumero(15));
    }
}
